from django.contrib import admin
from django_etl.models import *
from django import forms

class TransformationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TransformationForm, self).__init__(*args, **kwargs)
        self.fields['destination_model'].queryset = ContentType.objects.exclude(
            app_label__in=[
                'auth','contenttypes','sessions','sites',
                'admin','south','django_etl'])

class TransformationInline(admin.TabularInline):
	model = Transformation
        form = TransformationForm

class ExtractionAdmin(admin.ModelAdmin):
	inlines = [TransformationInline,]

def process_job(modeladmin, request, queryset):
    for elem in queryset.all():
    	elem.process()
process_job.short_description = "Process Job"

class JobAdmin(admin.ModelAdmin):
	list_display = ['extraction', 'input_data',]
	actions = [process_job,]
	readonly_fields = ['output',]

class TransformationAdmin(admin.ModelAdmin):
    form = TransformationForm

admin.site.register(SourceType)
admin.site.register(Extraction,ExtractionAdmin)
admin.site.register(FieldParser)
admin.site.register(Transformation,TransformationAdmin)
admin.site.register(Job,JobAdmin)

