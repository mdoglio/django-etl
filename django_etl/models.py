from django.db import models
import datetime
import csv
import cPickle
from django.contrib.contenttypes.models import ContentType

class SourceType(models.Model):
    INPUT_TYPE_CHOICES =(('1','file'),('2','string'))
    name = models.CharField(max_length=255, unique=True)
    input_type = models.CharField(max_length=1,choices=INPUT_TYPE_CHOICES)

    def __unicode__(self):
        return self.name

class Extraction(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, blank=True)
    source_name = models.CharField(max_length=255)
    source_type = models.ForeignKey(SourceType)

    def __unicode__(self):
        return self.name

class FieldParser(models.Model):
    name = models.CharField(max_length=255, unique=True)
    parsing_function = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.name

class Transformation(models.Model):
    extraction = models.ForeignKey(Extraction, related_name='transformations')
    input_field_name = models.CharField(max_length=255)
    input_field_parser = models.ForeignKey(FieldParser)
    output_field_name = models.CharField(max_length=255)
    destination_model = models.ForeignKey(ContentType, blank=True,null=True)
    destination_field = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.output_field_name

    class Meta:
        unique_together = ('extraction', 'output_field_name')

class Job(models.Model):
    extraction = models.ForeignKey(Extraction)
    input_data = models.FileField(upload_to='job_input_data')
    output = models.TextField(blank=True)

    def __unicode__(self):
        return "%s %s" % (self.extraction,self.input_data)

    def process(self):
        #only csv source file is supported 
        input = None
        if self.extraction.source_type.name == 'CSV file':
            input = csv.DictReader(self.input_data.file)
        
        converter = [(
                t.output_field_name,
                t.input_field_parser,
                t.input_field_name
            ) for t in self.extraction.transformations.all(
              ).select_related()]
        
        output = list()
        for row in input:
            output_row = dict()
            for name,parser,lookup in converter:
                output_row[name] = eval(parser.parsing_function % row[lookup])
            output.append(output_row)
        self.output = cPickle.dumps(output)
        self.save()
        

    
