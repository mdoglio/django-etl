# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SourceType'
        db.create_table('django_etl_sourcetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('input_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('django_etl', ['SourceType'])

        # Adding model 'Extraction'
        db.create_table('django_etl_extraction', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('source_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('source_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_etl.SourceType'])),
        ))
        db.send_create_signal('django_etl', ['Extraction'])

        # Adding model 'FieldParser'
        db.create_table('django_etl_fieldparser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('parsing_function', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('django_etl', ['FieldParser'])

        # Adding model 'Transformation'
        db.create_table('django_etl_transformation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('extraction', self.gf('django.db.models.fields.related.ForeignKey')(related_name='transformations', to=orm['django_etl.Extraction'])),
            ('input_field_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('input_field_parser', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_etl.FieldParser'])),
            ('output_field_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('destination_model', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'], null=True, blank=True)),
            ('destination_field', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal('django_etl', ['Transformation'])

        # Adding unique constraint on 'Transformation', fields ['extraction', 'output_field_name']
        db.create_unique('django_etl_transformation', ['extraction_id', 'output_field_name'])

        # Adding model 'Job'
        db.create_table('django_etl_job', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('extraction', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_etl.Extraction'])),
            ('input_data', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('output', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('django_etl', ['Job'])


    def backwards(self, orm):
        # Removing unique constraint on 'Transformation', fields ['extraction', 'output_field_name']
        db.delete_unique('django_etl_transformation', ['extraction_id', 'output_field_name'])

        # Deleting model 'SourceType'
        db.delete_table('django_etl_sourcetype')

        # Deleting model 'Extraction'
        db.delete_table('django_etl_extraction')

        # Deleting model 'FieldParser'
        db.delete_table('django_etl_fieldparser')

        # Deleting model 'Transformation'
        db.delete_table('django_etl_transformation')

        # Deleting model 'Job'
        db.delete_table('django_etl_job')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'django_etl.extraction': {
            'Meta': {'object_name': 'Extraction'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'source_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'source_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_etl.SourceType']"})
        },
        'django_etl.fieldparser': {
            'Meta': {'object_name': 'FieldParser'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'parsing_function': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'django_etl.job': {
            'Meta': {'object_name': 'Job'},
            'extraction': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_etl.Extraction']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_data': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'output': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'django_etl.sourcetype': {
            'Meta': {'object_name': 'SourceType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'django_etl.transformation': {
            'Meta': {'unique_together': "(('extraction', 'output_field_name'),)", 'object_name': 'Transformation'},
            'destination_field': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'destination_model': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'extraction': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transformations'", 'to': "orm['django_etl.Extraction']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_field_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'input_field_parser': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['django_etl.FieldParser']"}),
            'output_field_name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['django_etl']