from django.conf import settings
from django.db.models import Model, ForeignKey, ManyToManyField, OneToOneField

def get_models(*selected_apps):
    all_models = set()
    if selected_apps:
        apps = list(set(settings.INSTALLED_APPS) & set(selected_apps))
    else:
        apps = settings.INSTALLED_APPS
    for app in apps:
        try:
            models = __import__(app + '.models', {}, {}, 'models')
        except ImportError:
            continue
        for attr in dir(models):
            obj = getattr(models, attr)
            if isinstance(obj, object) and \
               hasattr(obj, '__bases__') and \
               Model in obj.__bases__:
                    all_models.add(obj)
    return all_models

def get_relns(all_models):
    foreignkeys = []
    one_to_one = []
    many_to_many = []
    for model in all_models:
        for field in model._meta.fields:
            if isinstance(field, OneToOneField): # must come before FK
                to = field.rel.to
                if to not in all_models:
                    raise ValueError
                print "%s o2o to %s" % (repr(model), repr(to))
                one_to_one.append((model, to))
            elif isinstance(field, ForeignKey):
                to = field.rel.to
                if to not in all_models:
                    raise ValueError
                print "%s fk to %s" % (repr(model), repr(to))
                foreignkeys.append((model, to))
            elif isinstance(field, ManyToManyField):
                to = field.rel.to
                if to not in all_models:
                    raise ValueError
                print "%s m2m to %s" % (repr(model), repr(to))
                many_to_many.append((model, to))
    return foreignkeys, one_to_one, many_to_many
                   
def sort_deps(models,one2many,one2one,many2many):
    import networkx as nx
    from networkx.algorithms import topological_sort
    
    DG = nx.DiGraph()
    for weight, relation in ((1,one2many),(2,one2one),(3,many2many)):
        DG.add_weighted_edges_from(
            (model_to,model_from,weight) 
                for model_from,model_to in relation)
    return topological_sort(DG)
